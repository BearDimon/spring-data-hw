package com.bsa.springdata.office;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OfficeService {
    private final OfficeRepository officeRepository;

    public OfficeService(OfficeRepository officeRepository) {
        this.officeRepository = officeRepository;
    }

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository.findAllByTechnology(technology)
                .stream()
                .map(OfficeDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        officeRepository.updateByAddressWithProject(oldAddress, newAddress);
        return officeRepository.findByAddress(newAddress).map(OfficeDto::fromEntity);
    }
}
