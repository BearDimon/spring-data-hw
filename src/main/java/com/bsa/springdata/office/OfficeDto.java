package com.bsa.springdata.office;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfficeDto {
    private UUID id;
    private String city;
    private String address;

    public static OfficeDto fromEntity(Office office) {
        return OfficeDto
                .builder()
                .id(office.getId())
                .address(office.getAddress())
                .city(office.getCity())
                .build();
    }
}
