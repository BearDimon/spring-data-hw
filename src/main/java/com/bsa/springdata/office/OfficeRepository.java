package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {
    @Query("SELECT o " +
            "FROM Office o " +
            "WHERE EXISTS ( " +
                "SELECT u " +
                "FROM User u " +
                "WHERE u.team.technology.name = :technology " +
                "AND u.office = o)")
    List<Office> findAllByTechnology(@Param("technology") String technology);

    @Modifying
    @Transactional
    @Query("UPDATE Office SET address = :new_addr WHERE address = :old_addr AND users.size <> 0")
    void updateByAddressWithProject(@Param("old_addr") String old_address, @Param("new_addr") String new_address);

    Optional<Office> findByAddress(String address);
}
