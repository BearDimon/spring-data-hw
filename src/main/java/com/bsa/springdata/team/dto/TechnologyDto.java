package com.bsa.springdata.team.dto;

import com.bsa.springdata.team.Technology;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TechnologyDto {
    private UUID id;
    private String name;
    private String description;
    private String link;

    public static TechnologyDto fromEntity(Technology technology) {
        return TechnologyDto
                .builder()
                .id(technology.getId())
                .name(technology.getName())
                .description(technology.getDescription())
                .link(technology.getLink())
                .build();
    }
}
