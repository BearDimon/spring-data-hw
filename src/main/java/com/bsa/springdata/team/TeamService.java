package com.bsa.springdata.team;

import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class TeamService {
    private final TeamRepository teamRepository;
    private final TechnologyRepository technologyRepository;

    public TeamService(TeamRepository teamRepository, TechnologyRepository technologyRepository) {
        this.teamRepository = teamRepository;
        this.technologyRepository = technologyRepository;
    }

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var teams = teamRepository.findAllByTechnologyAndDevs(oldTechnologyName, devsNumber);
        var technology = technologyRepository.findByName(newTechnologyName);
        technology.ifPresent(t -> teamRepository.saveAll(
                teams.stream()
                        .peek(p -> p.setTechnology(t))
                        .collect(Collectors.toList())
        ));
    }

    public void normalizeName(String hipsters) {
        teamRepository.normalizeName(hipsters);
    }
}
