package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    @Modifying
    @Transactional
    @Query(value = "UPDATE teams " +
            "SET name= ( " +
                "SELECT CONCAT(t.name, '_', p.name, '_', th.name) " +
                "FROM (teams t INNER JOIN projects p ON p.id = t.project_id) " +
                "INNER JOIN technologies th ON th.id = t.technology_id " +
                "WHERE t.name = :name)" +
            "WHERE name = :name", nativeQuery = true)
    void normalizeName(@Param("name") String hipsters);

    Optional<Team> findByName(String teamName);

    int countByTechnologyName(String name);

    @Query("SELECT t FROM Team t WHERE t.technology.name = :name AND t.users.size < :devs")
    List<Team> findAllByTechnologyAndDevs(@Param("name") String oldTechnologyName, @Param("devs") int devsNumber);
}
