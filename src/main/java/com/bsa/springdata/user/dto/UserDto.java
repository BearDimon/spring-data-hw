package com.bsa.springdata.user.dto;

import com.bsa.springdata.office.OfficeDto;
import com.bsa.springdata.team.dto.TeamDto;
import com.bsa.springdata.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDto {
    private UUID id;
    private String firstName;
    private String lastName;
    private int experience;
    private OfficeDto office;
    private TeamDto team;

    public static UserDto fromEntity(User user) {
        return UserDto
                .builder()
                .id(user.getId())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .experience(user.getExperience())
                .office(OfficeDto.fromEntity(user.getOffice()))
                .team(TeamDto.fromEntity(user.getTeam()))
                .build();
    }
}
