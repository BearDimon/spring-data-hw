package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    List<User> findAllByLastNameStartingWithIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office.city = :city ORDER BY u.lastName ASC")
    List<User> findAllByCity(@Param("city") String city);

    List<User> findAllByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("SELECT u FROM User u WHERE u.office.city = :city AND u.team.room = :room")
    List<User> findAllByRoomAndCity(String room, String city, Sort sort);

    int deleteByExperienceLessThan(int experience);
}
