package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("SELECT p, SUM(t.users.size) AS all_users " +
            "FROM Project p INNER JOIN Team t ON t.project = p " +
            "WHERE t.technology.name = :technology " +
            "GROUP BY p " +
            "ORDER BY all_users DESC")
    List<Project> findTop5ByTechnology(@Param("technology") String technology, Pageable pageable);

    @Query("SELECT p " +
            "FROM Project p INNER JOIN Team t ON t.project = p " +
            "WHERE p.teams.size IN (" +
                "SELECT MAX(pp.teams.size) " +
                "FROM Project pp) " +
            "GROUP BY p " +
            "HAVING SUM(t.users.size) >= ALL (" +
                "SELECT SUM(t.users.size) " +
                "FROM Project pp INNER JOIN Team t ON t.project = pp " +
                "WHERE pp.teams.size IN (" +
                    "SELECT MAX(pp.teams.size) " +
                    "FROM Project pp) " +
                    "GROUP BY pp) " +
            "ORDER BY p.name DESC")
    List<Project> findTheBiggest(Pageable pageable);

    @Query("SELECT COUNT(p) " +
            "FROM Project p " +
            "WHERE EXISTS (" +
                "SELECT u " +
                "FROM User u " +
                "WHERE u.team.project = p " +
                "AND (SELECT r FROM Role r WHERE r.name = :role) member of u.roles)")
    int countWithRole(@Param("role") String role);

    @Query(value = "SELECT p.name, COUNT(t.id) AS teamsNumber, (" +
                "SELECT COUNT(u.id) AS developersNumber " +
                "FROM users u " +
                "WHERE u.team_id IN (" +
                    "SELECT t.id " +
                    "FROM teams t " +
                    "WHERE t.project_id = p.id)), " +
            "STRING_AGG(th.name, ',') AS technologies " +
            "FROM (projects p INNER JOIN teams t ON p.id = t.project_id) " +
            "INNER JOIN technologies th ON th.id = t.technology_id " +
            "GROUP BY p.name, p.id " +
            "ORDER BY p.name", nativeQuery = true)
    List<ProjectSummaryDto> summary();
}