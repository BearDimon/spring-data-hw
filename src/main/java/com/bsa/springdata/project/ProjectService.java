package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.CreateProjectRequestDto;
import com.bsa.springdata.project.dto.ProjectDto;
import com.bsa.springdata.project.dto.ProjectSummaryDto;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.team.Technology;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<ProjectDto> findTop5ByTechnology(String technology) {
        return projectRepository.findTop5ByTechnology(technology, PageRequest.of(0, 5))
                .stream()
                .map(ProjectDto::fromEntity)
                .collect(Collectors.toList());
    }

    public Optional<ProjectDto> findTheBiggest() {
        return projectRepository.findTheBiggest(PageRequest.of(0, 1))
                .stream()
                .map(ProjectDto::fromEntity)
                .findAny();
    }

    public List<ProjectSummaryDto> getSummary() {
        return projectRepository.summary();
    }

    public int getCountWithRole(String role) {
        return projectRepository.countWithRole(role);
    }

    public UUID createWithTeamAndTechnology(CreateProjectRequestDto createProjectRequest) {
        var technology = Technology.builder()
                .name(createProjectRequest.getTech())
                .description(createProjectRequest.getTechLink())
                .link(createProjectRequest.getTechLink())
                .build();
        var team = Team.builder()
                .name(createProjectRequest.getTeamName())
                .area(createProjectRequest.getTeamArea())
                .room(createProjectRequest.getTeamRoom())
                .technology(technology)
                .build();
        var project = Project.builder()
                .name(createProjectRequest.getProjectName())
                .description(createProjectRequest.getProjectDescription())
                .teams(List.of(team))
                .build();

        return projectRepository.save(project).getId();
    }
}
